import pandas as pd
import time
from sklearn.model_selection import train_test_split
from Support_Vector_Classifier import * 

# importing the data set
data_set = pd.read_csv('data_set/Iris_Data.csv', header=None)

#  defining the coloumn names 
col_str  = 'sepal_length,sepal_width,petal_length,petal_width,species'
cols = col_str.split(',')

#  setting the column names in the data set
data_set.columns = cols

#  defining our target feature we want to predict
target = 'species'
target_feature_values = data_set[target].unique()

#  splitting our test and training data 
#  here we are using very small training set as it will take a lot of time 
#  to train a large data set with support vectors
test_data,training_data = train_test_split(data_set, test_size = 0.33, random_state = 42)
training_data.columns, test_data.columns = cols,cols

#  creating the data dictionary for our training and testing data 
#  that will be used to feed our svm for training and testing
train_data_dict = create_data_dict(training_data, target)
test_data_dict = create_data_dict(test_data, target)

# naming keys in our dict as 1 and -1
#  1 = 'Iris-setosa' and '-1' = 'Iris-versicolor'
train_data_dict[1] = np.array(train_data_dict.pop('Iris-setosa'))
train_data_dict[-1] = np.array(train_data_dict.pop('Iris-versicolor'))

test_data_dict[1] = np.array(test_data_dict.pop('Iris-setosa'))
test_data_dict[-1] = np.array(test_data_dict.pop('Iris-versicolor'))


#  start time for the training data
start_time = time.time()

train(train_data_dict)

#  end time for training data
end_time = time.time()

#  printing tim taken to train the data
time_taken = (end_time - start_time)/60
print('Time to train : ' , time_taken,' min')


#  testing our prediction
correct, wrong = 0,0
for key,value in test_data_dict.items() :
    ans = key
    for feature_set in value :
        prediction = predict(feature_set)

        if prediction == ans : 
            correct+=1
        else :
            wrong+=1    

acc = correct*100/ (correct+wrong)

print("correct : ",correct)
print('Wrong : ',wrong)
print("Total : ", (correct+wrong) )
print("Accuracy : ",acc, '%')

