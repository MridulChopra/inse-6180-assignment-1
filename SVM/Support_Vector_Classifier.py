import numpy as np

''' 
    This will train the svm for the given data. 
    for svm yi(x.w +b) -1 >= 0 must be satisfied for all the data 
    we want to minimize the |w| for vector w and maximize our b so that we 
    get optimal value for w and b 
'''
def train(data_set) :
    # initialize to global to we can 
    # reference thoughout  
    global data 
    data = data_set

    # will contains the magnitude of all w as key and [w,b] as values
    w_dict = {}

    #  we need to consider all the possible +ve and -ve scenarios since 
    #  the dot product with w will be affected by it 
    transformations = [ [1,1], [1,-1], [-1,1], [1,1] ]

    # contains all the values of all features in the data
    all_values = np.array([])
    for yi in data :
        all_values = np.append(all_values, data[yi] )

    #  find the min and max value from all the values
    max_feature_value = max(all_values)
    min_feature_value = min(all_values)

    #  these are the size or length of the step we will be using 
    #  to reach the global minimum in each iteration
    step_lengths = [
                        max_feature_value * 0.1,
                        max_feature_value * 0.01,
                        max_feature_value * 0.001
                    ]

    #  these are teh multiples of b and step multiples for b
    b_range_mul = 5 
    b_step_mul = 5 

    #  the optimized value 
    #  not optimize but will optimize in later steps 
    optimum = max_feature_value * 20

    for step_length in step_lengths  :
        
        current_w = np.array([optimum,optimum])

        isOptimized = False
        while not isOptimized : 

            #  increasing the value of b with small steps
            for current_b in np.arange(
                                    -1*max_feature_value*b_range_mul,
                                    max_feature_value*b_range_mul,
                                    step_length*b_step_mul
                                ) :

                # for every possible transformation of w 
                # we will calculate yi(xi.w + b) >= 1  
                #  if that is not satisfied for any data we have to skip
                #  because that must satisfy for all w 
                for t in transformations :

                    #  multipy w with the current tranformation to get 
                    #  transformation of w 
                    w_transformation = current_w*t

                    option_found = True

                    for yi in data : 
                        for xi in data[yi] :
                            if not yi* ( np.dot(w_transformation, xi) + current_b)  >= 1 :
                                option_found = False # in case w does not satisfy the option
                                break

                    # in case we found the w transfomation satisfies condition for
                    # all the data then add it to the dictionary 
                    if option_found : 
                        w_dict[np.linalg.norm(w_transformation)] = [w_transformation,current_b]


            # when value of w[0] or w[1] tends to 0, then 
            #  we are reaching the optimized value
            if current_w[0] < 0 : 
                isOptimized = True
            else : 
                current_w = current_w - step_length # decreasing the value of w 


        #  sorting all the key in the w_dict
        w_mags = sorted( [n for n in w_dict])

        #  minimun |w| is our optimal choice
        optimal_choice = w_dict[w_mags[0]]

        global w,b
        w = optimal_choice[0]
        b = optimal_choice[1]

        # for our next iteration we will optimize or selected w 
        # adding some step so that we are just ahead of it 
        optimum = optimal_choice[0][0] + step_length*2    

''' 
     This will help us make prediction for the svm 
     y = x.w + b 
'''
def predict(features) : 

    prediction =   np.dot(np.array(features), w) +b 
    return np.sign(prediction)


'''
    for the given data it will create a  dictionary 
    which will contain target feature values as key and their attributes as an array of arrays as value

    Note : here we are multiplying length and width of the sepal and petal 
    to get the area of sepal and petal and use that for prediction  
'''
def create_data_dict(data, target) :
    data_dict= {}
    #  iterate all rows in data
    for row in data.iterrows():
        row = row[1] 
        #  get the previously added key
        prev_arr = data_dict.get(row[target] , [] )

        #  making the new array of features
        arr = [[ 
                        row['sepal_length']*row['sepal_width'] , 
                        row['petal_length']*row['petal_width']
                ]]
        
        # adding that to our dictionary of data   
        data_dict.update({row[target] : prev_arr+arr })        
    return data_dict   





