import numpy as np
import pandas as pd

''' 
    Initialize all the values to be used through out
'''
def initialize(data_set, tar_feature, tar_feature_values, att) : 
    
    global data, target_feature, target_feature_values, attributes
    
    data = data_set
    target_feature = tar_feature
    target_feature_values = tar_feature_values
    attributes = att


'''
    calculate the occurences of the different feature values for the 
    target feature values and store them in a dict of dataframes
'''
def initialize_stats() :

    stat = {} # contains feature name as key and dataframe as values
    
    for feature in attributes : 

        att_values = data[feature].unique()  # different values attribute can have
        df = pd.DataFrame()

        # calculate the occurence of each value of feature and store in data frame
        for att_value in att_values :
            temp = {    
                        tar_value : len(data.loc[
                                                    ( data[feature] == att_value) & (data[target_feature] == tar_value)
                                                
                                                ])

                        for tar_value in target_feature_values 
                    }
            df[att_value] = temp.values()
        
        # renaming rows of the dataframe
        for i in range( len(target_feature_values) ) :
            df.rename( index = {i : target_feature_values[i]}, inplace = True )        
        
        stat[feature] = df #storing the dataframe in the stats

    global stats
    stats = stat # make stats as global variable


'''
    calculates the probability i.e. p(x|y)
'''
def probability(x ,y , x_value) :

    value = stats[x][x_value][y] # get the occurences of x for a given value

    #  if its the target attribute simply return
    #  occurences/total-occurences
    if x == target_feature : 
        return value/len(data)

    sum = stats[x].sum(axis = 1)[y] # sum of all types of occurences irrespective of value of x for a given value of target value
    return value/sum # probability




'''
    calculates the P(y | X) = P(y) * P(x1 | y ) * P(x2 | y) ....
    denominator is ignored as it will be same for all and will not affect in 
    the comparison  
'''
def posteriori(y , X) :

    prob =  probability(target_feature,y,y) # this simple probability i.e. p(y)

    # calculating P(x1 | y), P(x2 |y) and multipying
    for x, x_value in X.items() : 
        prob *= probability(x, y, x_value)

    return prob     

''' 
    predicts the naive bayes
'''
def predict(X) :

    #  contains probability of all possible outcomes or target classes
    probs = { 
                target_class : posteriori (target_class, X)
                for target_class in target_feature_values
            } 

    #  returning the one with max probability as the prediction
    for key,value in probs.items() : 
        if value == max(probs.values()) :
            return key



               
