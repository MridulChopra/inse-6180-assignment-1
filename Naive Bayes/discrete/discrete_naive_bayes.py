import pandas as pd
from discrete_bayes_utils import *

#  initialize the data set
data_set = pd.read_csv('discrete/data_set/play_golf_train.csv', header = None)

#  coloumn names of the data set
col_str = 'Outlook,Temperature Numeric,Temperature Nominal,Humidity Numeric,Humidity Nominal,Windy,Play'
col_names = col_str.split(',')

#  setting the coloumn names
data_set.columns = col_names

#  dropping the not required coloumns for the discrete classification
drop_cols = ['Temperature Numeric', 'Humidity Numeric']
col_names = list(filter( lambda item : item not in drop_cols, col_names))
data_set = data_set.drop(drop_cols, axis =1)

#  defining the target attribute to be predicted and its possible values
target_feature = 'Play'
target_feature_values = data_set[target_feature].unique()

#  initialize the different variables as global to be used through out
initialize(data_set, target_feature, target_feature_values, col_names)

#  initialize the stats to be used
initialize_stats()


# making the test data ready
test_data = pd.read_csv('discrete/data_set/play_golf_train.csv',header=None)
test_data.columns = col_str.split(',')
test_data = test_data.drop(drop_cols, axis =1)


correct,wrong = 0,0
#   testing our distribution
for i in range( len(test_data.index) ) : 
    
    test_case = { att : test_data.iloc[i][att] for att in col_names if att != target_feature}
    
    actual_result = test_data.iloc[i][target_feature]
    result = predict(test_case)

    if result == actual_result:
        correct+=1
    else :
        wrong+=1


print('Correct :', correct ,end = ", ")
print('Wrong :', wrong , end = ", ")

accuracy = correct *100 / (correct + wrong) 

print('Accuracy :', accuracy,'%')


