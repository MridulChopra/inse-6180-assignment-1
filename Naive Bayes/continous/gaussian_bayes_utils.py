import pandas as pd
import numpy as np


''' 
    Initialize the values that will be used throughout as global  
'''
def initialize(tar, tar_classes, data_set, tar_classes_prob) :
    
    global target, target_classes, data, target_classes_prob

    target = tar
    target_classes = tar_classes
    data = data_set
    target_classes_prob = tar_classes_prob

'''
    initializes mean, variance and standard deviation
    for all the attributes given a particular target class
'''
def create_gaussian_stats() :
    
    stat = {}  # dictonary containing mean, sd, and variance dataframes

    mean = pd.DataFrame()
    var = pd.DataFrame()
    std = pd.DataFrame()

    for target_class in target_classes : 

        # get the data which has only one value for the target attribute 
        condition = data[target] == target_class 
        df = data.loc[condition]

        # calculating mean, variance and standard deviation for each of the 
        # the attributes
        mean[target_class] = df.mean() 
        var[target_class] = df.var()
        std[target_class] = df.std()

    #  making the transpose of the dataframe 
    #  so that its easy to access the data
    mean = mean.T
    var = var.T
    std = std.T

    #  addding them to the dictonary
    stat['mean'] = mean
    stat['var'] = var
    stat['std'] = std

    # making it as a global variable as it will be used through out 
    global stats
    stats = stat   

    return stats

''' 
    predicts p(x|y) according to the gaussian distribution

'''
def gaussian_prob(x , y, x_value) :

    #  find the required stats for the gaussian formula for x and y 
    mean = stats['mean'][x][y]
    var = stats['var'][x][y]
    sd = stats['std'][x][y]

    prob = np.exp( -0.5 * ( (x_value - mean) / sd )**2 ) / np.sqrt(2*np.pi*var)

    return prob

'''
    calculates the P(y | X) = P(y) * P(x1 | y ) * P(x2 | y) ....
    denominator is ignored as it will be same for all and will not affect in 
    the comparison  
'''
def posteriori(y , X) :

    prob = target_classes_prob[y] # this simple probability i.e. p(y)

    # calculating P(x1 | y), P(x2 |y) and multipying
    for x, x_value in X.items() : 
        prob *= gaussian_prob(x, y, x_value)

    return prob     

''' 
    predicts the naive bayes
'''
def predict(X) :

    #  contains probability of all possible outcomes or target classes
    probs = { 
                target_class : posteriori (target_class, X)
                for target_class in target_classes 
            } 

    #  returning the one with max probability as the prediction
    for key,value in probs.items() : 
        if value == max(probs.values()) :
            return key

