import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from gaussian_bayes_utils import *

#  reading the data
data_set = pd.read_csv('continous/data_set/Iris_Data.csv', header= None)

# setting col names of the data
col_names = 'sepal_length,sepal_width,petal_length,petal_width,species'.split(',')
data_set.columns = col_names

#  dividing data into training and test data
training_data,test_data = train_test_split(data_set, test_size = 0.33, random_state = 42)

#  defining our target attribute and target value outcomes
target = 'species' 
target_classes = training_data[target].unique()

# findng the indivisual probility of each of the target attribute possible values
target_classes_prob =   {   
                            target_class : 
                            len(training_data[ training_data[target] == target_class ].index) /
                            len( training_data.index )

                            for target_class in target_classes
                        }

#  initialize the data to be used
initialize(target, target_classes, training_data, target_classes_prob)

#  creating mean, standard deviation and variance
create_gaussian_stats()

correct = 0
wrong = 0


#   testing our distribution
for i in range( len(test_data.index) ) : 
    
    test_case = { att : test_data.iloc[i][att] for att in col_names if att != target}
    
    actual_result = test_data.iloc[i][target]
    result = predict(test_case)

    if result == actual_result:
        correct+=1
    else :
        wrong+=1


print('Correct :', correct ,end = ", ")
print('Wrong :', wrong , end = ", ")

accuracy = correct *100 / (correct + wrong) 

print('Accuracy :', accuracy,'%')

