import pandas as pd
import numpy as np
from Node import Node


def initialize(tar, tar_attr_val, tar_ig, data, att) :
    
    global dataSet, target,target_attr_values,attributes , target_ig  
    
    dataSet = data
    target = tar
    target_attr_values = tar_attr_val
    attributes = att
    target_ig = tar_ig

def isHomegenous(attr, val) :

    universalCondition = True
    ptr = attr
    
    # the attribute may have some parent attribute 
    # in the decesion tree, so the occurences will depend upon 
    # the parent node as well.
    # Adding that in the conditions
    while ptr.parent is not None : 
        if ptr.branch is not None : 
            universalCondition &= dataSet[ptr.parent.name] == ptr.branch 
            ptr = ptr.parent


    condition = dataSet[attr.name] == val
    condition &= universalCondition

    df = dataSet.loc[condition]

    val = df[target].unique()
    
    if len(val) == 1 :
        return val[0]
    else : 
        return None    


'''
    return gain of the required attribute
    attr : attribute we want to calculate gain for 
    target_ig : the information gain of the target attribute
'''
def gain(attr) :
    return target_ig - entroppy(attr)


''' 
    calculate entroppy of the given attribute 
    attr : attribute for which we want to calculate entroppy for
'''
def entroppy(attr) :
    values = attr.values # get name of the attribute
    targetVals = target_attr_values # get the target attribute's values

    universalCondition = True
    ptr = attr
    
    # the attribute may have some parent attribute 
    # in the decesion tree, so the occurences will depend upon 
    # the parent node as well.
    # Adding that in the conditions
    while ptr.parent is not None : 
        if ptr.branch is not None : 
            universalCondition &= dataSet[ptr.parent.name] == ptr.branch 
            ptr = ptr.parent

    # A dict of dict which contains 
    # key contains all the possible values of the attr
    # value contains a dict of possible value of target attr values as target value and number of occ as value for the attr
    table = dict()
    
    for value in values : 
        row = dict() # a row of the table
 
        for val in targetVals : 

            # adding conditions for getting required info
            condition = dataSet[attr.name] == value
            condition &= dataSet[target] == val  
            condition &= universalCondition

            df = dataSet.loc[condition]
            row.update( { val : len(df.index) } )

        table.update( { value : row } ) # adding row the table

    #  calculating the entroppy of the attribute
    entroppy = 0
    for key,value in table.items() : 
        entroppy += information_gain(value) * probability(key, table)

    return entroppy    
          
'''
    return the information gain of a given attribute
    attr : a dict of occurences of the target class for the given attribute
'''
def information_gain(attr) :
    
    #  calculating the sum of the occurences
    total = sum( attr.values() )  

    ig = 0

    # calculating the information gain for each attribute and adding them
    for value in attr.values() : 
        if value != 0 :
            ig += value/total * np.log2(value/total) * -1
    
    return ig


'''
    return the probability of a given attribute 
    attr : the attribute we want to find probability for
    table : dict of dict consisting of all the attributes and number of occurences of each target value
'''
def probability(attr, table) :
    
    #  calculate sum of all the occurences
    total = 0  
    for row in table.values() :
        total += sum( row.values() )

    # get the required attribute 
    attrRow = table.get(attr)
    occ = sum( attrRow.values() ) # sum of all the occurences of the attribute 

    return occ/total   # return probability


''' 
    add the nodes to the selected root node. 
    In short it builds the decesion tree.
'''
def add_nodes(attr) : 
    
    #  Iterate over all the possible values 
    #  i.e. we are finding the childeren to the branch of the node
    for val in attr.values : 
        
        #  check if the result values for the given condition are homongenous
        is_homogenous = isHomegenous(attr, val)

        #  if its homongenous
        # add the value as the child of that node 
        if is_homogenous : 
            attr.children.update({val : is_homogenous})
        

        #  if value is not homogenoues, then we find the other attribute
        #  that is best fit for the given branch
        else : 

            if len(attributes) == 0 :
                
                # if there are not attributes left in the list to be used
                # then we shortlist the values based upon probability 
                add_target_node(attr,val)
            
            else :     
                
                gains = {} # contains gains for all the attributes
                selected_attr = None

                for sub_attr in attributes : 
                    sub_attr.parent = attr # set current node as parent node of all others
                    sub_attr.branch = val # set which branch our selected node will belong to
                    gains.update( {sub_attr :gain(sub_attr)} ) # adding gain of each node

                #  shortlisting best node based on 
                #  maximum gain 
                for att,attr_gain in gains.items()  :
                    if attr_gain == max( gains.values() ) :
                        selected_attr = att

                # updating the children of the current node
                # selected node is child of the current node 
                attr.children.update( {val : selected_attr} )
                
                # remove the selected node
                #  from the list of nodes to be use as
                #  its added to the tree
                attributes.remove(selected_attr)
                
                #  repeat the same process for the selected node
                add_nodes(selected_attr)
                


def add_target_node(attr,val) :
    
    occ = {}  # contains the class occurences of the given attribute

    universalCondition = True
    ptr = attr

    # an attribute has parent
    # we have to extract occurences based upon that
    while ptr.parent is not None : 
        if ptr.branch is not None : 
            universalCondition &= dataSet[ptr.parent.name] == ptr.branch 
            ptr = ptr.parent

    #  updating the target attr value occurences for the given attribute
    for att_val in target_attr_values :
        condition = dataSet[attr.name] == val
        condition &= dataSet[target] == att_val
        df = dataSet.loc[condition]
        occ.update({ att_val : len(df.index) })

    # print('node : ', attr.name, ', branch  ',val,' ,  ' ,occ )

    for k,v in occ.items() : 
        if v == max( occ.values() ) :
            attr.children.update({val : k }) 
            break      
    
'''
Helps to predict values based on the made decesion tree
'''
def  predict(node, item) :

    feature = node.name
    # print("checking ", feature)
    value = item.get(feature)


    prediction = node.children.get(value)

    if type(prediction) is Node : 
        return predict(prediction, item)
    else : 
        return prediction     
    