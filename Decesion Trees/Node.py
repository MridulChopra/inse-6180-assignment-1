class Node :

    def __init__(self, name, parent, values) :
        self.parent = parent
        self.name = name
        self.values = values
        self.branch = None
        self.init_childeren()


    def init_childeren(self) :
        self.children = { val : None for val in self.values} 
 

