from Utils import *
from sklearn.model_selection import train_test_split


#  read all the data from the file
dataSet = pd.read_csv('data_set/car_evaluation.csv', header=None)

#  rename all the columns
col_names = ['buying', 'maint', 'doors', 'persons', 'lug_boot', 'safety', 'class']
# col_names = ['age','comp','type','class']
dataSet.columns = col_names

#  split data into training and test data
training_data,test_data = train_test_split(dataSet, test_size = 0.33, random_state = 42)

target_attr = 'class' # target attribute which we want to classify

# getting all the possible values for the target attribute
target_attr_values = dataSet[target_attr].unique()

# contains all the attributes as key and list of possible values as columns
attributes = { col : dataSet[col].unique() for col in list(dataSet) }

# for key,value in attributes.items() :
#     print(key, " : ", value)

# contains frequency of each target attribute possible values
target_values = { att:0 for att in target_attr_values}

# updating the target_values with real values
for attr in target_attr_values : 
    df = dataSet.loc[ dataSet[target_attr] == attr ]
    target_values.update( {attr : len(df.index) })

target_ig = information_gain(target_values)

#  all the attributes will be treated as the node of decesion tree
#  making list of all such nodes except the target attribute
attrs = []
for col in col_names : 
    if col != target_attr :
        attr = Node(col, None,attributes.get(col))
        attrs.append(attr)

# for item in attrs : 
#     print(item.values)


#  initialize our attributes, target attribute an data set to be used
initialize(target_attr, target_attr_values, target_ig, training_data, attrs)

#  gain of each attribute
gains = { attr : gain(attr) for attr in attrs}

selected_root_attr = ''

#  shortlisting the root node based on maximun gain
for attr,attr_gain in gains.items()  :
    if attr_gain == max( gains.values() ) :
        selected_root_attr = attr


attrs.remove(selected_root_attr) # remove the root node from the list of nodes
add_nodes(selected_root_attr) # add the nodes to the root node i.e. make the tree



#  maiking the test data ready
tests = []
for i in range(len(test_data.index)) :
    tests.append( test_data.iloc[i] )



#  correct and wrong predicted values
correct = 0
wrong = 0 

#  predicting the all the test data cases and evaluating them
for item in tests : 
    prediction = predict(selected_root_attr, item)
    actual = item.get(target_attr)

    if actual == prediction : 
        correct+=1
    else : 
        wrong+=1 

print( " Correct  : " , correct )
print( " wrong : ", wrong)

accuracy = correct/ (correct+wrong) *100

print()
print("Accuracy is : ", accuracy, "%")


