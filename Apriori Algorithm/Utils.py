from itertools import permutations,combinations
from collections.abc import Iterable

'''
    Read the Apripri data from the given file
    and return the data. 
'''
def readData(fileName) :
    dataFile = open(fileName, "r")
    data = dataFile.readlines()
    data = list(map( lambda line : line[0:-1] , data)) # removing \n from the end of each line
    data = list(map( lambda line : line.split(",") , data)) # converting each line into list of items
    dataFile.close()
    return data

''' write the result into the file '''
def writeData(rules)   :
    
    result = []
    for key,value in rules.items() :
        strg = ','.join(key[0])
        strg+= " ---> "
        strg += ','.join(key[1])
        strg+= " ( Confidence : "
        strg+=  "{:.2f}".format(value*100) + "% )"
        result.append(strg+" \n")

    file = open("Output.txt", "w")
    file.writelines(result)
    file.close()    

''' 
    get list of all items in the given dictionary
'''
def getItemsList(data) :
    items = set()
    for key in data : 
        if type(key) is not str and isinstance(key, Iterable) :
            for item in key :
                items.add(item)
        else :    
            items.add(key)
    return items

''' 
    returns the support count of the item set in the given
    data for the given number of combinations

    data : will be used to find the support count for each item
    itemList : the list of items we want to make combinations of
    combinations : maximum number of items that can be combined together
    minSupport : minimum support each item should have
'''
def getSupportCount(data, itemList, comb , minSupport = 0) :
    
    if type(itemList) is dict : 
        itemList = getItemsList(itemList)

    cList = list(combinations(itemList,comb))
    cListFreq = dict()
    for combination in cList :
        freq = 0
        for line in data :  
            if(set(combination).issubset(set(line))) :
                freq +=1
        if(freq >= minSupport) :     
            cListFreq.update({combination:freq})
    return cListFreq        

''' 
    Checks if any item in a are present in b or not
    if any item present then return true ; else false 
'''
def isItemRepeated(a,b) :
  return bool ( set(a).intersection(set(b)) )    


''' 
    Given the support table, it gives confidence for the relation
'''
def confidence(relation, supportTable) :
    a = relation[0]
    b = relation[1]
    
    # confidence(A->B) = support(A U B)/support(A)
    sA = findSupport(a, supportTable) # support of A
    sAUsB = findSupport(a+b, supportTable) # support of A U B
    
    # take care we don't divide by zero
    conf =  0 if sA == 0 else float(sAUsB/sA) 
    return conf

''' 
    Helps in finding support of an item
    item = tuple
    supportTable = dict 
'''
def findSupport(item, supportTable) :
    #  if length of item is 1, 
    # means item is present as a string in supportTable
    if len(item) == 1 :
        return supportTable.get(item[0],0)
    
    # if tuple contains more than one items,
    #  then there might be some other combination of these items
    #  present as keys in the dict
    item = tuple( sorted(item) )
    return supportTable.get(item,0) 

''' check rule eligibility '''
def isEligibleRule(rule, minConfidence, supportTable) : 
    if not isItemRepeated(rule[0],rule[1])  : # if item is repeated then its not a valid assosciation rule
            conf = confidence(rule,supportTable)  # get confidence of the valid rule
            if conf > minConfidence :
                 return True
    return False
