from AprioriExt import *


MIN_SUPPORT_PERCENT = 0.03222
MIN_CONFIDENCE = 0.25

MIN_SUPPORT = 0



data = readData('data_set/groceries.csv')
MIN_SUPPORT = MIN_SUPPORT_PERCENT * len(data)
supportTable = dict()

freqItems = getItemsFreq(data,MIN_SUPPORT)
prevFreqItems = dict()

items = getItemsList(data)
print("Found ", len(items), " items in the data set !!")
print()


i = 2
while freqItems :

    prevFreqItems = dict(freqItems)
    supportTable.update(prevFreqItems)
    
    
    # print(prevFreqItems)
    freqItems = getSupportCount2(data,freqItems,i,MIN_SUPPORT)
    
    i+=1

print("Completed all the iterations")
print("             Minimun Support  : ",MIN_SUPPORT_PERCENT*100,"%")
print()
finalFreqItemSet = prevFreqItems


print("Locating Assosciations....")
print("             Min Confidence : ",MIN_CONFIDENCE*100,"%")
print()



rules = assosciationRules(finalFreqItemSet,supportTable,MIN_CONFIDENCE)

print("Writing all the rules in the output file")
print()

writeData(rules)

print("Done writing the rules. Please check output.txt for the result")
print()
