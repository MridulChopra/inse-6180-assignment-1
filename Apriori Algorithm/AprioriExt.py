import threading
from itertools import permutations,combinations
from Utils import * 

rules = dict()

''' Will update the apriori rules '''
def updateAssosciationRules(newRules) :
    rules.update(newRules)

''' calculates the rules given the permutation list '''
def calculateRules(perms, minConfidence, supportTable) :
    
    lock = threading.Lock()
    threads = []
    step = 2000

    for i in range(0, len(perms), step) :
        thread =  threading.Thread(target=sortEligibleRules, args=(perms[i:i+step], minConfidence, supportTable, lock))  
        threads.append(thread)

    for thread in threads :
        thread.start()
    
    for thread in threads : 
        thread.join()
    


def sortEligibleRules(rules, minConfidence, supportTable, lock) :
    eligibleRules =  {rule : confidence(rule,supportTable) for rule in rules if isEligibleRule(rule, minConfidence, supportTable ) }
    lock.acquire()
    updateAssosciationRules(eligibleRules)
    lock.release()

''' 
    returns a dictionary containing
    item name as key and frequency as 
    value in the data provided
'''
def getItemsFreq(data , minSupport = 0) :
    items = dict() # stores the unique items
    for line in data : # iterating over each transaction in data
        for item in line :  # iterating over each item in a transaction
            freq = items.get(item , 0) # get the frequency of the item
            freq+=1 # increase its frequency
            items.update({item : freq}) # update the dictionary
    
    # removing the elements in the items
    # which have support < minSupport
    itemsCopy = dict(items)
    for(key, value) in itemsCopy.items() :
        if value < minSupport :
            items.pop(key)
    
    return items        


''' 
    gives all the possible assosiation rules satisfying min confidence
'''
def assosciationRules(itemList, supportTable, minConfidence = 0) :
    
    if type(itemList) is dict : # if data type is dictionary then get list of all items 
      itemList = getItemsList(itemList)

    subsets = [] # calculating all the possible subsets of the list of items
    for i in range(1 , len(itemList)) :
        sub = combinations(itemList,i)
        subsets += list(sub)
    
    perms = permutations(subsets,2) # getting all the possible permutations of the list of subsets
    perms = list(perms)
    #   find all the rules required
    calculateRules(perms, minConfidence, supportTable)
    
    return rules


''' 
    returns the support count of the item set in the given
    data for the given number of combinations

    data : will be used to find the support count for each item
    itemList : the list of items we want to make combinations of
    combinations : maximum number of items that can be combined together
    minSupport : minimum support each item should have
'''
def getSupportCount2(data, itemList, comb , minSupport = 0) :
    
    if type(itemList) is dict : 
        itemList = getItemsList(itemList)
   
    cList = list(combinations(itemList,comb))
    cListFreq = { tuple( sorted(item) ) : 0  for item in cList}

    for line in data : 
        combList = list(combinations(line,comb))
        for item in combList : 
            item = tuple( sorted(item) )
            if item in cListFreq.keys() : 
                freq = cListFreq.get(item) 
                freq+=1
                cListFreq.update({item:freq})
    
    return {k:v for(k,v) in cListFreq.items() if v>minSupport }


