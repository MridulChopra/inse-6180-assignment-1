1.  To Run the Apriori Algorithm 
    1.1  Open terminal 
    1.2  run cd "Apriori Algoritm"
    1.3  run python3 Apriori.py

2.  To Run the Decesion Trees 
    2.1 Open terminal
    2.2 run cd "Decesion Trees"
    2.3 run python3 DecesionTree.py

3. To Run Naive Bayes
    3.1 Running Continous Naive Bayes 
        3.1.1 Open terminal 
        3.1.2. run cd "Naive Bayes"/continous
        3.1.3 run python3 gaussian_naive_bayes.py
    3.1 Running Discrete Naive Bayes 
        3.1.1 Open terminal 
        3.1.2. run cd "Naive Bayes"/discrete
        3.1.3 run python3 discrete_naive_bayes.py
 
 4. To run the SVM   
    4.1 Open terminal
    4.2 run cd SVM
    4.3 run SVM_Train.py         
